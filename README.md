### Home Rental Project ###

Holiday Cottages/Homes/Flats Rental Website

#### Functionalities ####

1. Users (signin, login, favorites, preferences, ...)
2. Multicriteria search : location, room number, price, ...
3. Admin
4. Stats

#### Technologies ####

+ Java/JEE - Spring
+ Hibernate
+ HTML (jsp, jstl tags), CSS, JS

+ [Less Css][13] - Find here a [plugin][21] for Netbeans ! ;-)
+ [CoffeeScript][14]
+ [thymeleaf][15] - Template framework

### Maps API ###

+ The doc on the Maps API is [here][16]
+ The doc on the Static Maps API is [here][17]

#### To read ####

+ [Inversion of Control][1] (For more informations, read [this][4])
+ [Spring Framework][2] and [this][6]
+ [Aspect-Oriented Programming][3]
+ Introduction au framework Spring : [cours 1][5] | [cours 2][7] | [cours 3][8]
+ Hibernate : [cours 1][9] | [cours 2][11] | [cours 3][12]
+ [Spring Framework General Reference][10]
+ [Java Persistence API][18]]
+ [Spring Transaction Management][19]
+ [Haversine Fromula](http://en.wikipedia.org/wiki/Haversine_formula)
+ [MySQL Google Search API - Find location in neighborhood](https://developers.google.com/maps/articles/phpsqlsearch_v3)

[1]: http://fr.wikipedia.org/wiki/Spring_framework
[2]: http://fr.wikipedia.org/wiki/Spring_framework
[3]: http://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_aspect
[4]: http://www.martinfowler.com/articles/injection.html
[5]: http://ego.developpez.com/spring/
[6]: http://w3blog.fr/2009/10/09/framework-spring/
[7]: http://yannart.developpez.com/java/spring/tutoriel/
[8]: http://w3blog.fr/2010/10/18/realisation-projet-spring/
[9]: http://w3blog.fr/2008/08/21/framework-hibernate/
[10]: http://www.tutorialspoint.com/spring/spring_overview.htm
[11]: http://www.dzone.com/tutorials/java/spring/spring-hibernate-integration-1.html
[12]: http://www.mkyong.com/spring/maven-spring-hibernate-mysql-example/
[13]: https://github.com/marceloverdijk/lesscss-maven-plugin
[14]: https://github.com/iron9light/coffeescript-maven-plugin
[15]: http://www.thymeleaf.org/documentation.html
[16]: https://developers.google.com/maps/documentation/javascript/?hl=fr
[17]: https://developers.google.com/maps/documentation/staticmaps/?hl=fr
[18]: http://en.wikibooks.org/w/index.php?title=Java_Persistence&stable=1
[19]: http://static.springsource.org/spring/docs/2.0.x/reference/transaction.html
[20]: https://phpmyadmin.alwaysdata.com/
[21]: http://plugins.netbeans.org/plugin/32782/lesscss-module
